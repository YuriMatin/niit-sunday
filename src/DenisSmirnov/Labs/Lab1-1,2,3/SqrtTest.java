package ru.smirnov.learning;

import org.junit.Assert;
import org.junit.Test;

public class SqrtTest {

    @Test
    public void test() {
        double delta = 0.000000001;
        Assert.assertEquals(3.0, new Sqrt(9, delta).calc(), delta);
    }

}
