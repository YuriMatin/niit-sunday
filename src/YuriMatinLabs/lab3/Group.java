package lab3;


import java.util.ArrayList;

/**
 * Лабораторная №3
 * <p>
 * Данные о группах будут храниться в файле в виде строки
 * по следующему образцу: "ЯРиЭУ_2_170000_170001,170002..."
 * Используя метод split("_") создам из строки массив строк, в котором:
 * [0]-это название группы
 * [1]-это идентификатор группы
 * [2]-это идентификатор старосты. Если старосты нет, то будет записано 000000
 * [3]-это строка из индексов студентов
 */


public class Group {

    private final int groupID;
    private final String groupNAME;
    ArrayList<Student> studentsReferences = new ArrayList<>();
    Student headReference;
    int headID;

    //Конструктор
    public Group(String groupNAME, int groupID) {
        this.groupNAME = groupNAME;
        this.groupID = groupID;
    }

    public int getGroupID() {
        return groupID;
    }

    public String getGroupNAME() {
        return groupNAME;
    }


    //Добавление студента в группу
    public void addStudent(Student student) {
        studentsReferences.add(student);
        student.groupReference = this;
        student.groupID = this.getGroupID();
    }


    //Назначение старосты
    public void headAppointment(Student student) {
        if (this.searchStudentByID(student.getID())== null) {
            System.out.println("Студент зачислен в другую группу и не может быть старостой этой!");
        } else if (student.numberOfMarks() < 5) {
            System.out.println("У старосты не может быть незакрытых предметов!");
        } else if (headReference != null) {
            headReference.head = false;
            headReference = student;
            student.head = true;
            this.headID = student.getID();
        } else {
            headReference = student;
            student.head = true;
            this.headID = student.getID();
        }
    }

    //Поиск старосты
    public Student headSearch() {
        return headReference;
    }

    //Поиск студента по ИД
    public Student searchStudentByID(int studentID) {
        if (Integer.toString(studentID).length() < 6) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < studentsReferences.size(); i++)
            if (studentID == (studentsReferences.get(i)).getID()) {
                return studentsReferences.get(i);
            }
        return null;
    }

    // Поиск студента по ФИО
    public Student searchStudentByFIO(String studentFIO) {
        if (studentFIO.equals("")) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < studentsReferences.size(); i++) {
            if (studentFIO.equals((studentsReferences.get(i)).getFIO())) {
                return studentsReferences.get(i);
            }
        }
        return null;
    }

    //Вычисление среднего балла в группе
    public double calcAverageMarkInGroup() {
        double marksSum = 0;
        for (int i = 0; i < studentsReferences.size(); i++) {
            marksSum += studentsReferences.get(i).calcAverageMark();
        }
        return marksSum / studentsReferences.size();
    }

    //Вычисление количества оценок на группу
    /*Нужно для подсчета статистики*/
    public int calcAmountOfMarksInGroup() {
        int amountOfMarks = 0;
        for (int i = 0; i < studentsReferences.size(); i++) {
            amountOfMarks += studentsReferences.get(i).numberOfMarks();
        }
        return amountOfMarks;
    }

    //Вычисление максимально возможного количества оценок на группу
    /*Нужно для подсчета статистики*/
    public int calcMaxAmountOfMarksInGroup() {
        return studentsReferences.size() * 5;
    }

    //Исключение студента из группы
    public void expulsionFromGroup(Student student) {
        if (this.searchStudentByID(student.getID())!=null) {
            this.studentsReferences.remove(student);
            student.groupReference = null;
            student.groupID = 0;
        } else {
            System.out.println("Указанный студент не найден! Проверьте правильность введенных данных");
        }
    }

    @Override
    public String toString() {
        if(headReference == null){
            return "Группа " + "<< "+groupNAME +" >> "+ "ID: "+ getGroupID() +" староста группы: не назначен"+", количество студенов в группе: "+ Integer.toString(studentsReferences.size());
        }
        return "Группа " +"<< "+ groupNAME +" >> "+ "ID: "+ getGroupID() +" староста группы: "+headReference.getFIO()+", количество студенов в группе: "+ Integer.toString(studentsReferences.size());
    }
}
