/**
 * Руководитель направления. Осуществляет управление над всеми проектами.
 * Считаю, что данный класс должен наследоваться не от проект-менеджера, а
 * от класса "работник" и реализовывать только интерфейс "Heading", поскольку
 * участия в проектах он, по сути, не принимает, только руководит и ставит задачи
 */
public class SeniorManager extends Employee implements Heading{
    String[] projectsNames;
    int budget;
    int numOfParticiples;

    public SeniorManager(int ID, String name) {
        super(ID, name);
        this.position = "руководитель направления";
    }


    @Override
    public int getHeadingBonus() {
        return numOfParticiples*(budget/100);
    }

    @Override
    void calcPayment() {
        this.payment=getHeadingBonus()+getHeadingBonus()/10;
    }

    @Override
    public String toString() {
        return "Сотрудник "+getName()+" ID: "+getID()+"; занимаемая должность: "
                +position+"; начисленная зарплата: "+Double.toString(getPayment())+" рублей" ;
    }
}
