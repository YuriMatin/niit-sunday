/**
 * Расчет оплаты исходя из участия в проекте
 * (бюджет проекта делится пропорционально персональному вкладу).
 */
public interface Project {
    //Оплата за участие в проекте
    double getProjectBonus();
}
