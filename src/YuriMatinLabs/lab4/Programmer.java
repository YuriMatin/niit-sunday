/**
 * Инженер-программист
 */
public class Programmer extends Engineer {


    public Programmer(int ID, String name, int workTme, int BASE,
                      String projectName,
                      int projectBudget,
                      double participleInProject) {
        super(ID, name, workTme, BASE, projectName, projectBudget, participleInProject);
        this.position = "программист";
    }

}
