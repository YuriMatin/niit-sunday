import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.PortUnreachableException;


/**
 * Стартовое окно с кнопками запуска сервера и клиента
 */
public class StartWindow extends Application {

    public void exceptionMessage(String message) {
        Stage stage = new Stage();
        BorderPane sRoot = new BorderPane();
        sRoot.setCenter(new Label(message));
        Scene scene = new Scene(sRoot, 200, 100);
        stage.setScene(scene);
        stage.show();
    }


    public static void main(String[] args) throws Exception {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("Launcher window");
        BorderPane root = new BorderPane();

        //Запуск сервера с указанием порта
        VBox serverVBox = new VBox();
        serverVBox.setSpacing(5);
        serverVBox.setPadding(new Insets(10));
        HBox serverPortField = new HBox();
        serverPortField.setSpacing(6);
        TextField serverPort = new TextField();
        serverPort.setPromptText(" порт");
        serverPortField.getChildren().addAll(new Label("Укажите порт"), serverPort);
        Button createServer = new Button("Запустить сервер");
        serverVBox.getChildren().addAll(serverPortField, createServer);
        createServer.setOnAction(event -> new Thread(() -> {
            try {
                new Server(Integer.parseInt(serverPort.getText()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start());

        //Запуск клиента с указанием порта и имени
        VBox clientVBox = new VBox();
        clientVBox.setSpacing(5);
        BorderPane.setMargin(clientVBox,new Insets(10));
        HBox clientPortField = new HBox();
        clientPortField.setSpacing(6);
        HBox clientNameField = new HBox();
        clientNameField.setSpacing(6);
        TextField port = new TextField();
        port.setPromptText(" порт");
        TextField name = new TextField();
        name.setPromptText("Имя клиента(не обязательно)");
        clientPortField.getChildren().addAll(new Label("Укажите порт"), port);
        clientNameField.getChildren().addAll(new Label("Имя клиента"), name);
        Button createClient = new Button("Новый клиент");
        createClient.setCenterShape(true);
        createClient.setOnAction(event -> {
            try {
                new Client(Integer.parseInt(port.getText()), name.getText());
            } catch (NumberFormatException e) {
                exceptionMessage("Некорректные данные!");
            } catch (PortUnreachableException e){
                exceptionMessage("Сервер не запущен!");
            }  catch(IOException e) {
                exceptionMessage("Что-то пошло не так!");
            }
        });
        clientVBox.getChildren().addAll(clientPortField, clientNameField, createClient);

        root.setLeft(serverVBox);
        root.setRight(clientVBox);
        primaryStage.setOnCloseRequest(event -> System.exit(0));

        Scene scene = new Scene(root, 600, 500);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
