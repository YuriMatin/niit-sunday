/**
 * Created by user.1 on 02.07.2017.
 */
public class CollatzMain {
    static int step;

    public static void main(String args[]) {
        int stepMax = 0;
        int max = 1;

        for (int i = 1; i <= 1000000; i++) {
            Collatz(i);
            if (step > stepMax) {
                stepMax = step;
                max = i;
            }
            step = 0;
        }

        System.out.println("Для числа " + max + " последовательность пришла к единице за " + stepMax + " шагов");
    }

    public static void Collatz(int j) {
        if (j == 1) return;
        step++;
        if (j % 2 == 0)
            Collatz(j / 2);
        else if (j % 2 == 1)
            Collatz (j * 3 + 1);
    }

}
